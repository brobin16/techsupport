﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupportDatabase.Model;
using TechSupport.Controller;

namespace TechSupport.View
{
    public partial class frmCreateIncident : Form
    {
        private IncidentController inController;
        public Incident incident;
        public frmCreateIncident()
        {
            InitializeComponent();
            inController = new IncidentController();
            try
            {
                List<Customer> customerList = this.inController.GetCustomers();
                customerComboList.DataSource = customerList;
                customerComboList.DisplayMember = "CustomerName";
                customerComboList.ValueMember = "CustomerID";

                List<Product> productList = this.inController.GetProducts();
                productComboList.DataSource = productList;
                productComboList.DisplayMember = "ProductName";
                productComboList.ValueMember = "ProductCode";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void PutIncidentData(Incident incident)
        {
            incident.CustomerID = customerComboList.SelectedValue.ToString();
            incident.ProductCode = productComboList.SelectedValue.ToString();
            incident.DateOpened = DateTime.Today;
            incident.Title = titleBox.Text;
            incident.Description = descriptionBox.Text;
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCreateIncident_Click(object sender, EventArgs e)
        {
            if (IsValidData())
            {
                incident = new Incident();
                this.PutIncidentData(incident);
                try
                {
                    this.inController.addIncident(incident);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Your incident has been added.");
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
            {
                MessageBox.Show("Please complete all required fields to add the incident.");
            }
        }

        private bool IsValidData()
        {
            if (Validator.IsPresent(customerComboList) &&
                Validator.IsPresent(productComboList) &&
                Validator.IsPresent(titleBox) &&
                Validator.IsPresent(descriptionBox))
            {
                return true;
            }
            else
                return false;
        }

    }
}

