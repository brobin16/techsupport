﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport
{
    public partial class IncidentsView : Form
    {
        public IncidentsView()
        {
            InitializeComponent();
        }

        View.frmCreateIncident createIncidentForm;
        private void createIncident(object sender, EventArgs e)
        {
            if (createIncidentForm == null)
            {
                createIncidentForm = new TechSupport.View.frmCreateIncident();
                createIncidentForm.MdiParent = this;
                createIncidentForm.FormClosed += createIncident_FormClosed;
                createIncidentForm.Show();
            }
            else
                createIncidentForm.Activate();
        }

        private void createIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
           createIncidentForm = null;
        }


        View.frmUpdateIncident updateIncidentForm;
        private void updateIncident(object sender, EventArgs e)
        {
            if (updateIncidentForm == null)
            {
                updateIncidentForm = new TechSupport.View.frmUpdateIncident();
                updateIncidentForm.MdiParent = this;
                updateIncidentForm.FormClosed += updateIncident_FormClosed;
                updateIncidentForm.Show();
            }
            else
                updateIncidentForm.Activate();
        }
        private void updateIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            updateIncidentForm = null;
        }

        frmProductMaintenance productMaintForm;
        private void displayOpenIncidents(object sender, EventArgs e)
        {
            if (productMaintForm == null)
            {
                productMaintForm = new TechSupport.frmProductMaintenance();
                productMaintForm.MdiParent = this;
                productMaintForm.FormClosed += productMaintForm_FormClosed;
                productMaintForm.Show();
            }
            else
                productMaintForm.Activate();
        }

        private void productMaintForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            productMaintForm = null;
        }


        View.frmViewOpenIncidentByTechnician viewOpenIncidentByTechnicianForm;
        private void displayOpenIncidentsByTechnician(object sender, EventArgs e)
        {
            if (viewOpenIncidentByTechnicianForm == null)
            {
                viewOpenIncidentByTechnicianForm = new TechSupport.View.frmViewOpenIncidentByTechnician();
                viewOpenIncidentByTechnicianForm.MdiParent = this;
                viewOpenIncidentByTechnicianForm.FormClosed += displayOpenIncidentsByTechnician_FormClosed;
                viewOpenIncidentByTechnicianForm.Show();
            }
            else
                viewOpenIncidentByTechnicianForm.Activate();
        }

        private void displayOpenIncidentsByTechnician_FormClosed(object sender, FormClosedEventArgs e)
        {
            viewOpenIncidentByTechnicianForm = null;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void IncidentsView_Load(object sender, EventArgs e)
        {

        }




        View.frmDisplayIncidentsReport displayIncidentsReportForm;
        private void displayIncidentsReport(object sender, EventArgs e)
        {
            if (displayIncidentsReportForm == null)
            {
                displayIncidentsReportForm = new TechSupport.View.frmDisplayIncidentsReport();
                displayIncidentsReportForm.MdiParent = this;
                displayIncidentsReportForm.FormClosed += displayIncidentsReport_FormClosed;
                displayIncidentsReportForm.Show();
            }
            else
                displayIncidentsReportForm.Activate();
        }
        private void displayIncidentsReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            displayIncidentsReportForm = null;
        }
      
    }

  
}
