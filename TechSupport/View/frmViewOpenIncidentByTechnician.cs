﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupportDatabase.Model;
using TechSupport.Controller;

namespace TechSupport.View
{
    public partial class frmViewOpenIncidentByTechnician : Form
    {
        private IncidentController inController;
        private List<Technician> technicianList;
        private List<Incident> incidentList;

        public frmViewOpenIncidentByTechnician()
        {
            InitializeComponent();
            inController = new IncidentController();
        }

        private void frmViewOpenIncidentByTechnician_Load(object sender, EventArgs e)
        {
            try
            {
                technicianList = inController.GetTechniciansWithOpenIncidents();
                technicianNameComboBox.DataSource = technicianList;
                this.GetTechnicianIncidents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
            
        }
        private void GetTechnicianIncidents()
        {

            int techID = (int)technicianNameComboBox.SelectedValue;
            try
            {
                incidentList = inController.GetTechnicianIncidents(techID);
                incidentDataGridView.DataSource = incidentList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void technicianNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (technicianNameComboBox.SelectedIndex < 0)
            {
                return;
            }
            try
            {
            Technician technician = technicianList[technicianNameComboBox.SelectedIndex];
            technicianBindingSource.Clear();
            technicianBindingSource.Add(technician);
             this.GetTechnicianIncidents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

 
    }
}
