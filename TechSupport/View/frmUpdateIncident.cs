﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupportDatabase.Model;
using TechSupport.Controller;

namespace TechSupport.View
{
    public partial class frmUpdateIncident : Form
    {
        private Incident incident;
        private Incident currentIncident;
        private IncidentController inController;
        private List<Technician> technicianList;

        public frmUpdateIncident()
        {
            InitializeComponent();
            inController = new IncidentController();
            this.Load += frmUpdateIncident_Load;
        }

        private void frmUpdateIncident_Load(object sender, EventArgs e)
        {
            this.LoadAllTechnicians();
            comboBoxTechnician.SelectedIndex = -1;
            txtBoxTextToAdd.Enabled = false;
            btnUpdate.Enabled = false;
            btnClose.Enabled = false;
        }

        private void LoadAllTechnicians()
        {
            try
            {
                technicianList = this.inController.GetTechnicians();
                comboBoxTechnician.DataSource = technicianList;
                comboBoxTechnician.DisplayMember = "TechnicianName";
                comboBoxTechnician.ValueMember = "TechID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void OpenIncident(int incidentID)
        {
            incident = this.getIncident(incidentID);
            boxCustomer.Text = incident.CustomerName;
            boxProduct.Text = incident.ProductName;
            comboBoxTechnician.Text = incident.TechnicianName;
            txtBoxTitle.Text = incident.Title;
            string dateOpened = incident.DateOpened.ToShortDateString();
            txtBoxDateOpened.Text = dateOpened;
            txtBoxDescription.Text = incident.Description;
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            if (IsValidIncidentID())
            {
                try
                    {
                    if (this.inController.OpenIncident(Convert.ToInt32(boxIncidentID.Text)).DateClosed == "")
                    {
                        this.OpenIncident(Convert.ToInt32(boxIncidentID.Text));
                        btnGet.Enabled = true;
                        btnUpdate.Enabled = true;
                        btnClose.Enabled = true;
                        txtBoxTextToAdd.Enabled = true;
                        comboBoxTechnician.Enabled = true;
                    }
                    else
                    {
                        this.OpenIncident(Convert.ToInt32(boxIncidentID.Text));
                        btnGet.Enabled = true;
                        btnUpdate.Enabled = false;
                        btnClose.Enabled = false;
                        txtBoxTextToAdd.Enabled = false;
                        comboBoxTechnician.Enabled = false;
                    }
                 
                }
                catch (NullReferenceException ex)
                {
                    MessageBox.Show("Not a valid Incident ID.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private bool IsValidIncidentID()
        {
            if (Validator.ValidateIncidentID(boxIncidentID))
            {
                return true;
            }
            else
                return false;
        }

        private bool ValidateTechnicianSelected()
        {
            if (Validator.ValidateTechnician(comboBoxTechnician))
            {
                return true;
            }
            else
                return false;
        }

        private Incident getIncident(int incidentID)
        {
            return this.inController.OpenIncident(incidentID);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            this.currentIncident = this.getIncident(Int32.Parse(boxIncidentID.Text));
            if (IsValidIncidentID() && ValidateTechnicianSelected())
            {
                String date = System.Environment.NewLine + "<" + DateTime.Today.ToString("d") + "> ";
                String updatedDescription = "";
                if (this.CompareIncidentChanged(this.incident, this.currentIncident)) 
                {
                    try
                    {
                        if (this.incident.TechnicianName != comboBoxTechnician.Text && txtBoxTextToAdd.Text == "")
                        {
                            updatedDescription = date + "updated/assigned the technician";
                        }
                        else if (txtBoxTextToAdd.Text == "")
                        {
                            MessageBox.Show("Please enter text in the text to add field");
                        }
                        else
                        {
                            updatedDescription = date + txtBoxTextToAdd.Text;
                        }

                        if (txtBoxDescription.Text.Length == 200 || txtBoxDescription.Text.Length + date.Length > 200)
                        {
                            MessageBox.Show("Current description is 200 characters. Can not add more text to description. ");
                            txtBoxTextToAdd.Text = String.Empty;
                            return;
                        }
                        else if (txtBoxDescription.Text.Length + updatedDescription.Length > 200)
                        {
                            DialogResult dialog = MessageBox.Show("Additional text is too long for max description limit of 200 characters. Would you like to truncate?", "Confirm", MessageBoxButtons.YesNo);
                            if (dialog == DialogResult.Yes)
                            {
                               int maxAdditionalDescription = 200 - (txtBoxDescription.Text.Length + date.Length);
                               txtBoxTextToAdd.Text = TruncateTextToAdd(txtBoxTextToAdd.Text, maxAdditionalDescription);
                                MessageBox.Show("Please try to update again");
                                return;
                            }
                            else if (dialog == DialogResult.No)
                            {
                                MessageBox.Show("Please update description to fit 200 characters restriction.");
                                return;
                            }
                        } else
                        {
                            this.inController.updateIncident(Int32.Parse(boxIncidentID.Text), Int32.Parse(comboBoxTechnician.SelectedValue.ToString()), updatedDescription);
                            txtBoxTextToAdd.Text = String.Empty;
                            this.OpenIncident(Convert.ToInt32(boxIncidentID.Text));
                        }          
                    }
                    catch (FormatException ex)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                {
                    MessageBox.Show("This incident has already been changed by another user. Please refresh the incident in order to make an update.");
                }
            }
        }

        private bool CompareIncidentChanged (Incident originalIncident, Incident currentIncident)
        {
            if (incident.CustomerID != currentIncident.CustomerID)
            {
                return false;
            }
            else if (incident.ProductCode != currentIncident.ProductCode)
            {
                return false;
            }
            else if (incident.TechID != currentIncident.TechID)
            {
                return false;
            }
            else if (incident.DateOpened != currentIncident.DateOpened)
            {
                return false;
            }
            else if (incident.DateClosed != currentIncident.DateClosed)
            {
                return false;
            }
            else if (incident.Title != currentIncident.Title)
            {
                return false;
            }
            else if (incident.Description != currentIncident.Description)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private string TruncateTextToAdd(string value, int maximumDescriptionLength)
        {
            return value?.Substring(0, Math.Min(value.Length, maximumDescriptionLength));
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.currentIncident = this.getIncident(Int32.Parse(boxIncidentID.Text));
            if (IsValidIncidentID() && ValidateTechnicianSelected())
            {
                if (this.CompareIncidentChanged(this.incident, this.currentIncident))
                {
                    try
                    {
                        String date = System.Environment.NewLine + "<" + DateTime.Today.ToString("d") + "> ";
                        String updatedDescription = "";

                        if (this.incident.TechnicianName != comboBoxTechnician.Text && txtBoxTextToAdd.Text == "")
                        {
                            updatedDescription = date + "updated/assigned the technician";
                        }
                        else if (this.inController.OpenIncident(Int32.Parse(boxIncidentID.Text)).TechnicianName != "" && txtBoxTextToAdd.Text == ""
                            && txtBoxDescription.Text.Length + date.Length < 200 || txtBoxTextToAdd.Text == "")
                        {
                            updatedDescription = "";
                        }
                        else if (txtBoxDescription.Text.Length == 200)
                        {
                            MessageBox.Show("Current description is 200 characters. Incident will be closed with no associated text.");
                            txtBoxTextToAdd.Text = String.Empty;
                            updatedDescription = "";
                        }
                        else if (txtBoxDescription.Text.Length + date.Length + txtBoxTextToAdd.Text.Length > 200)
                        {
                            DialogResult closeDialog = MessageBox.Show("Additional text is too long for max description limit of 200 characters. Would you like to truncate?", "Confirm", MessageBoxButtons.YesNo);
                            if (closeDialog == DialogResult.Yes)
                            {
                                int maxAdditionalDescription = 200 - (txtBoxDescription.Text.Length + date.Length);
                                txtBoxTextToAdd.Text = TruncateTextToAdd(txtBoxTextToAdd.Text, maxAdditionalDescription);
                                MessageBox.Show("Please try to close again with shortened text.");
                                return;
                            }
                            else if (closeDialog == DialogResult.No)
                            {
                                MessageBox.Show("Incident will be closed with no associated text.");
                                txtBoxTextToAdd.Text = String.Empty;
                                updatedDescription = "";
                            }
                        }
                        else
                        {
                            updatedDescription = date + txtBoxTextToAdd.Text;
                        }
                                DialogResult dialog = MessageBox.Show("The incident cannot be updated once it is closed. Are you sure you would like to close it?", "Close Incident", MessageBoxButtons.YesNo);
                                if (dialog == DialogResult.No)
                                {
                                    return;
                                }
                                else if (dialog == DialogResult.Yes)
                                {
                                this.inController.closeIncident(Int32.Parse(boxIncidentID.Text), Int32.Parse(comboBoxTechnician.SelectedValue.ToString()), updatedDescription);
                                 txtBoxTextToAdd.Text = String.Empty;
                                 this.OpenIncident(Convert.ToInt32(boxIncidentID.Text));
                                  btnGet.Enabled = true;
                                  btnUpdate.Enabled = false;
                                  btnClose.Enabled = false;
                                  txtBoxTextToAdd.Enabled = false;
                                  comboBoxTechnician.Enabled = false;
                        }
                        }
                    catch (FormatException ex)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
                else
                {
                    MessageBox.Show("This incident has already been changed by another user. Please refresh the incident in order to make an update.");
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
