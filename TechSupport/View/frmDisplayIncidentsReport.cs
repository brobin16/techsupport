﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupport.View
{
    public partial class frmDisplayIncidentsReport : Form
    {
        public frmDisplayIncidentsReport()
        {
            InitializeComponent();
        }

        private void frmDisplayIncidentsReport_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'techSupportReportDataSet.IncidentsByProductsAndTechnician' table. You can move, or remove it, as needed.
            this.incidentsByProductsAndTechnicianTableAdapter.Fill(this.techSupportReportDataSet.IncidentsByProductsAndTechnician);

            this.reportViewer1.RefreshReport();
        }
    }
}
