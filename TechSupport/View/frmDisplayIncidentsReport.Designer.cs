﻿namespace TechSupport.View
{
    partial class frmDisplayIncidentsReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.techSupportReportDataSet = new TechSupport.TechSupportReportDataSet();
            this.techSupportReportDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.IncidentsByProductsAndTechnicianBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.incidentsByProductsAndTechnicianTableAdapter = new TechSupport.TechSupportReportDataSetTableAdapters.IncidentsByProductsAndTechnicianTableAdapter();
            this.tableAdapterManager = new TechSupport.TechSupportReportDataSetTableAdapters.TableAdapterManager();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.techSupportReportDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.techSupportReportDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsByProductsAndTechnicianBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // techSupportReportDataSet
            // 
            this.techSupportReportDataSet.DataSetName = "TechSupportReportDataSet";
            this.techSupportReportDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // techSupportReportDataSetBindingSource
            // 
            this.techSupportReportDataSetBindingSource.DataSource = this.techSupportReportDataSet;
            this.techSupportReportDataSetBindingSource.Position = 0;
            // 
            // IncidentsByProductsAndTechnicianBindingSource
            // 
            this.IncidentsByProductsAndTechnicianBindingSource.DataMember = "IncidentsByProductsAndTechnician";
            this.IncidentsByProductsAndTechnicianBindingSource.DataSource = this.techSupportReportDataSet;
            // 
            // incidentsByProductsAndTechnicianTableAdapter
            // 
            this.incidentsByProductsAndTechnicianTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = TechSupport.TechSupportReportDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "ReportIncidentsByProductsAndTechnicianDataSet";
            reportDataSource1.Value = this.IncidentsByProductsAndTechnicianBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "TechSupport.ReportIncidentsByProductsAndTechnician.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(984, 662);
            this.reportViewer1.TabIndex = 1;
            // 
            // frmDisplayIncidentsReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 662);
            this.Controls.Add(this.reportViewer1);
            this.Name = "frmDisplayIncidentsReport";
            this.Text = "Display Incidents by Products and Technician";
            this.Load += new System.EventHandler(this.frmDisplayIncidentsReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.techSupportReportDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.techSupportReportDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncidentsByProductsAndTechnicianBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource techSupportReportDataSetBindingSource;
        private TechSupportReportDataSet techSupportReportDataSet;
        private System.Windows.Forms.BindingSource IncidentsByProductsAndTechnicianBindingSource;
        private TechSupportReportDataSetTableAdapters.IncidentsByProductsAndTechnicianTableAdapter incidentsByProductsAndTechnicianTableAdapter;
        private TechSupportReportDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}