﻿using System;

namespace TechSupport.View
{
    partial class frmUpdateIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGet = new System.Windows.Forms.Button();
            this.lblIncidentID = new System.Windows.Forms.Label();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTechnician = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblDateOpened = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblTextToAdd = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.boxIncidentID = new System.Windows.Forms.TextBox();
            this.boxCustomer = new System.Windows.Forms.TextBox();
            this.boxProduct = new System.Windows.Forms.TextBox();
            this.txtBoxTitle = new System.Windows.Forms.TextBox();
            this.txtBoxDescription = new System.Windows.Forms.TextBox();
            this.txtBoxTextToAdd = new System.Windows.Forms.TextBox();
            this.txtBoxDateOpened = new System.Windows.Forms.TextBox();
            this.comboBoxTechnician = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnGet
            // 
            this.btnGet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGet.Location = new System.Drawing.Point(342, 25);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(75, 23);
            this.btnGet.TabIndex = 0;
            this.btnGet.Text = "Get";
            this.btnGet.UseVisualStyleBackColor = true;
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // lblIncidentID
            // 
            this.lblIncidentID.AutoSize = true;
            this.lblIncidentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIncidentID.Location = new System.Drawing.Point(46, 25);
            this.lblIncidentID.Name = "lblIncidentID";
            this.lblIncidentID.Size = new System.Drawing.Size(85, 16);
            this.lblIncidentID.TabIndex = 1;
            this.lblIncidentID.Text = "Incident ID:";
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCustomer.Location = new System.Drawing.Point(46, 57);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(77, 16);
            this.lblCustomer.TabIndex = 2;
            this.lblCustomer.Text = "Customer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(46, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Product:";
            // 
            // lblTechnician
            // 
            this.lblTechnician.AutoSize = true;
            this.lblTechnician.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTechnician.Location = new System.Drawing.Point(46, 134);
            this.lblTechnician.Name = "lblTechnician";
            this.lblTechnician.Size = new System.Drawing.Size(88, 16);
            this.lblTechnician.TabIndex = 4;
            this.lblTechnician.Text = "Technician:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(46, 168);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(43, 16);
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "Title:";
            // 
            // lblDateOpened
            // 
            this.lblDateOpened.AutoSize = true;
            this.lblDateOpened.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOpened.Location = new System.Drawing.Point(46, 205);
            this.lblDateOpened.Name = "lblDateOpened";
            this.lblDateOpened.Size = new System.Drawing.Size(104, 16);
            this.lblDateOpened.TabIndex = 6;
            this.lblDateOpened.Text = "Date Opened:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(46, 243);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(91, 16);
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = "Description:";
            // 
            // lblTextToAdd
            // 
            this.lblTextToAdd.AutoSize = true;
            this.lblTextToAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextToAdd.Location = new System.Drawing.Point(46, 321);
            this.lblTextToAdd.Name = "lblTextToAdd";
            this.lblTextToAdd.Size = new System.Drawing.Size(97, 16);
            this.lblTextToAdd.TabIndex = 8;
            this.lblTextToAdd.Text = "Text To Add:";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(252, 397);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(353, 397);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(457, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // boxIncidentID
            // 
            this.boxIncidentID.BackColor = System.Drawing.SystemColors.MenuBar;
            this.boxIncidentID.Location = new System.Drawing.Point(163, 25);
            this.boxIncidentID.Name = "boxIncidentID";
            this.boxIncidentID.Size = new System.Drawing.Size(134, 20);
            this.boxIncidentID.TabIndex = 12;
            // 
            // boxCustomer
            // 
            this.boxCustomer.BackColor = System.Drawing.SystemColors.MenuBar;
            this.boxCustomer.Location = new System.Drawing.Point(163, 57);
            this.boxCustomer.Name = "boxCustomer";
            this.boxCustomer.ReadOnly = true;
            this.boxCustomer.Size = new System.Drawing.Size(369, 20);
            this.boxCustomer.TabIndex = 13;
            // 
            // boxProduct
            // 
            this.boxProduct.BackColor = System.Drawing.SystemColors.MenuBar;
            this.boxProduct.Location = new System.Drawing.Point(163, 95);
            this.boxProduct.Name = "boxProduct";
            this.boxProduct.ReadOnly = true;
            this.boxProduct.Size = new System.Drawing.Size(369, 20);
            this.boxProduct.TabIndex = 14;
            // 
            // txtBoxTitle
            // 
            this.txtBoxTitle.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtBoxTitle.Location = new System.Drawing.Point(163, 168);
            this.txtBoxTitle.Name = "txtBoxTitle";
            this.txtBoxTitle.ReadOnly = true;
            this.txtBoxTitle.Size = new System.Drawing.Size(369, 20);
            this.txtBoxTitle.TabIndex = 15;
            // 
            // txtBoxDescription
            // 
            this.txtBoxDescription.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtBoxDescription.Location = new System.Drawing.Point(163, 243);
            this.txtBoxDescription.Multiline = true;
            this.txtBoxDescription.Name = "txtBoxDescription";
            this.txtBoxDescription.ReadOnly = true;
            this.txtBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxDescription.Size = new System.Drawing.Size(369, 53);
            this.txtBoxDescription.TabIndex = 16;
            // 
            // txtBoxTextToAdd
            // 
            this.txtBoxTextToAdd.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBoxTextToAdd.Location = new System.Drawing.Point(163, 321);
            this.txtBoxTextToAdd.Multiline = true;
            this.txtBoxTextToAdd.Name = "txtBoxTextToAdd";
            this.txtBoxTextToAdd.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxTextToAdd.Size = new System.Drawing.Size(369, 61);
            this.txtBoxTextToAdd.TabIndex = 17;
            // 
            // txtBoxDateOpened
            // 
            this.txtBoxDateOpened.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtBoxDateOpened.Location = new System.Drawing.Point(163, 201);
            this.txtBoxDateOpened.Name = "txtBoxDateOpened";
            this.txtBoxDateOpened.ReadOnly = true;
            this.txtBoxDateOpened.Size = new System.Drawing.Size(134, 20);
            this.txtBoxDateOpened.TabIndex = 18;
            // 
            // comboBoxTechnician
            // 
            this.comboBoxTechnician.BackColor = System.Drawing.SystemColors.ControlLight;
            this.comboBoxTechnician.FormattingEnabled = true;
            this.comboBoxTechnician.Location = new System.Drawing.Point(163, 128);
            this.comboBoxTechnician.Name = "comboBoxTechnician";
            this.comboBoxTechnician.Size = new System.Drawing.Size(369, 21);
            this.comboBoxTechnician.TabIndex = 19;
            // 
            // frmUpdateIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 686);
            this.Controls.Add(this.comboBoxTechnician);
            this.Controls.Add(this.txtBoxDateOpened);
            this.Controls.Add(this.txtBoxTextToAdd);
            this.Controls.Add(this.txtBoxDescription);
            this.Controls.Add(this.txtBoxTitle);
            this.Controls.Add(this.boxProduct);
            this.Controls.Add(this.boxCustomer);
            this.Controls.Add(this.boxIncidentID);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblTextToAdd);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblDateOpened);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblTechnician);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCustomer);
            this.Controls.Add(this.lblIncidentID);
            this.Controls.Add(this.btnGet);
            this.Name = "frmUpdateIncident";
            this.Text = "Update Incident";
            this.Load += new System.EventHandler(this.frmUpdateIncident_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.Label lblIncidentID;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTechnician;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblDateOpened;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblTextToAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox boxIncidentID;
        private System.Windows.Forms.TextBox boxCustomer;
        private System.Windows.Forms.TextBox boxProduct;
        private System.Windows.Forms.TextBox txtBoxTitle;
        private System.Windows.Forms.TextBox txtBoxDescription;
        private System.Windows.Forms.TextBox txtBoxTextToAdd;
        private System.Windows.Forms.TextBox txtBoxDateOpened;
        private System.Windows.Forms.ComboBox comboBoxTechnician;
    }
}