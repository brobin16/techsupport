﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TechSupport.Controller;
using TechSupportDatabase.Model;

namespace TechSupport
{
    public partial class frmProductMaintenance : Form
    {

        private IncidentController inController;
        public frmProductMaintenance()
        {
            InitializeComponent();
            inController = new IncidentController();
        }

        private void frmProductMaintenanceLoad(object sender, EventArgs e)
        {
            List<Incident> incidentList;
            try
            {
                incidentList = this.inController.GetIncidents();

                if (incidentList.Count > 0)
                {
                    Incident incident;
                    for (int i = 0; i < incidentList.Count; i++)
                    {
                        incident = incidentList[i];
                        lvIncidents.Items.Add(incident.ProductCode);
                        lvIncidents.Items[i].SubItems.Add(incident.DateOpened.ToShortDateString());
                        lvIncidents.Items[i].SubItems.Add(incident.CustomerName.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.TechnicianName.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Title.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("All incidents are complete");
                    this.BeginInvoke(new MethodInvoker(Close));
                }
            }
            catch (Exception ex)
            {
                 MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }
    }
}
        