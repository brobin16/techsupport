﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportDatabase.Model;

namespace TechSupport.Controller
/// <summary>
/// The controller class deals with the DAL and delegates the work to DAL
/// </summary>
/// 
{
   public class IncidentController
    {
        public IncidentController()
        {
            //testing to add local commit   
        }

        public List<Incident> GetIncidents()
        {
            return TechSupportDatabase.DAL.IncidentDAL.GetIncidents();
        }

        public List<Customer> GetCustomers()
        {
            return TechSupportDatabase.DAL.CustomerDAL.GetCustomers();
        }

        public List<Product> GetProducts()
        {
            return TechSupportDatabase.DAL.ProductDAL.GetProducts();
        }

        public int addIncident(Incident incident)
        {
            return TechSupportDatabase.DAL.IncidentDAL.AddIncident(incident);

        }

        public Incident OpenIncident(int incidentID)
        {
            return TechSupportDatabase.DAL.IncidentDAL.OpenIncident(incidentID);
        }

        public List<Technician> GetTechnicians()
        {
            return TechSupportDatabase.DAL.TechnicianDAL.GetTechnicians();
        }

       public int updateIncident(int incident, int techID, string description)
        {
            return TechSupportDatabase.DAL.IncidentDAL.UpdateIncident(incident, techID, description);
        }

        public int closeIncident(int incident, int techID, string description)
        {
            return TechSupportDatabase.DAL.IncidentDAL.CloseIncident(incident, techID, description);
        }

        public List<Technician> GetTechniciansWithOpenIncidents()
        {
            return TechSupportDatabase.DAL.TechnicianDAL.GetTechniciansWithOpenIncidents();
        }

        public List<Incident> GetTechnicianIncidents(int TechID)
        {
            return TechSupportDatabase.DAL.IncidentDAL.ViewIncidents(TechID);
        }

    }
}

