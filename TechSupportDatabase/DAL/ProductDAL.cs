﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportDatabase.Model;

namespace TechSupportDatabase.DAL

{
    public static class ProductDAL
    {

        public static List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();
            string selectStatement =
                "SELECT Products.Name AS ProductName, ProductCode " +
                "FROM Products ";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Product product = new Product();
                                product.ProductName = reader["ProductName"].ToString();
                                 product.ProductCode = reader["ProductCode"].ToString();
                                 productList.Add(product);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return productList;
        }
    }
}
