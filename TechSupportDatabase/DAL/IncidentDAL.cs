﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using TechSupportDatabase.Model;
using System.Windows.Forms;

namespace TechSupportDatabase.DAL
{
    /// <summary>
    /// This the DAL class that deals with incidents.
    /// </summary>
    public static class IncidentDAL
    {
        public static List<Incident> GetIncidents()
        {
            List<Incident> incidentList = new List<Incident>();
            string selectStatement =
                "SELECT ProductCode, DateOpened, Customers.Name AS CustomerName, " +
                "Technicians.Name AS TechnicianName, Title, DateClosed " +
                "FROM Incidents " +
                "JOIN Customers ON Incidents.CustomerID = Customers.CustomerID " +
                "LEFT JOIN Technicians ON Incidents.TechID = Technicians.TechID " +
               "WHERE DateClosed IS NULL " +
                "ORDER BY DateOpened";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Incident incident = new Incident();
                                incident.ProductCode = reader["ProductCode"].ToString();
                                incident.DateOpened = (DateTime)reader["DateOpened"];
                                incident.CustomerName = reader["CustomerName"].ToString();
                                incident.TechnicianName = reader["TechnicianName"].ToString();
                                incident.Title = reader["Title"].ToString();
                                incidentList.Add(incident);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return incidentList;
        }


        public static int AddIncident(Incident incident)
        {
            SqlConnection connection = IncidentDBConnection.GetConnection();
            string insertStatement =
              "INSERT Incidents " +
               "(ProductCode, CustomerID, DateOpened, Title, Description) " +
              "VALUES (@ProductCode, @CustomerID, @DateOpened, @Title, @Description)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@ProductCode", incident.ProductCode);
            insertCommand.Parameters.AddWithValue("@CustomerID", incident.CustomerID);
            insertCommand.Parameters.AddWithValue("@DateOpened", incident.DateOpened);
            insertCommand.Parameters.AddWithValue("@Title", incident.Title);
            insertCommand.Parameters.AddWithValue("@Description", incident.Description);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                string selectStatement =
                  "SELECT IDENT_CURRENT('Incidents') FROM Incidents";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                int incidentID = Convert.ToInt32(selectCommand.ExecuteScalar());
                return incidentID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<Incident> ViewIncidents(int TechID)
        {
            List<Incident> incidentList = new List<Incident>();
            String selectStatement =
               "SELECT Incidents.ProductCode, Products.Name AS ProductName, " +
                "DateOpened, DateClosed, Customers.Name AS CustomerName, " +
                "Title, Description " +
                "FROM Incidents " +
                "JOIN Products ON Incidents.ProductCode = Products.ProductCode " +
                "JOIN Customers ON Incidents.CustomerID = Customers.CustomerID " +
                "WHERE TechID = @TechID AND DateClosed IS NULL";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@TechID", TechID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Incident incident = new Incident();
                                incident.ProductCode = reader["ProductCode"].ToString();
                                incident.ProductName = reader["ProductName"].ToString();
                                incident.DateOpened = (DateTime)reader["DateOpened"];
                                incident.DateClosed = reader["DateClosed"].ToString();
                                incident.CustomerName = reader["CustomerName"].ToString();
                                incident.Title = reader["Title"].ToString();
                                incident.Description = reader["Description"].ToString();
                                incidentList.Add(incident);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return incidentList;
        }

        public static Incident OpenIncident(int incidentID)
        {
            Incident incident = new Incident();
            String selectStatement =
               "SELECT Products.Name AS ProductName, " +
                "DateOpened, DateClosed, Customers.Name AS CustomerName, " +
                "Technicians.Name AS TechnicianName, Title, Description " +
                "FROM Incidents " +
                "JOIN Products ON Incidents.ProductCode = Products.ProductCode " +
                "JOIN Customers ON Incidents.CustomerID = Customers.CustomerID " +
                "LEFT JOIN Technicians ON Incidents.TechID = Technicians.TechID " +
                "WHERE IncidentID = @IncidentID";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@IncidentID", incidentID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                incident.ProductName = reader["ProductName"].ToString();
                                incident.DateOpened = (DateTime)reader["DateOpened"];
                                incident.DateClosed = reader["DateClosed"].ToString();
                                incident.CustomerName = reader["CustomerName"].ToString();
                                incident.TechnicianName = reader["TechnicianName"].ToString();
                                incident.Title = reader["Title"].ToString();
                                incident.Description = reader["Description"].ToString();
                            }
                            else
                            {
                                incident = null;
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return incident;
        }

    public static int UpdateIncident(int IncidentID, int TechID, string Description)
        {
            SqlConnection connection = IncidentDBConnection.GetConnection();
            string updateStatement =
                "UPDATE Incidents " +
                "SET TechID = @TechID, Description += @Description " +
                "WHERE IncidentID = @IncidentID";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@IncidentID", IncidentID);
            updateCommand.Parameters.AddWithValue("@TechID", TechID);
            updateCommand.Parameters.AddWithValue("@Description", Description);
            try
            {
                connection.Open();
                updateCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('Incidents') FROM Incidents";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                int incidentID = Convert.ToInt32(selectCommand.ExecuteScalar());
                return incidentID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static int CloseIncident(int IncidentID, int TechID, string Description)
        {
            SqlConnection connection = IncidentDBConnection.GetConnection();
            string updateStatement =
                "UPDATE Incidents " +
                "SET TechID = @TechID, Description += @Description, DateClosed = @DateClosed " +
                "WHERE IncidentID = @IncidentID";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@IncidentID", IncidentID);
            updateCommand.Parameters.AddWithValue("@TechID", TechID);
            updateCommand.Parameters.AddWithValue("@Description", Description);
            updateCommand.Parameters.AddWithValue("@DateClosed", DateTime.Today);
            try
            {
                connection.Open();
                updateCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('Incidents') FROM Incidents";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                int incidentID = Convert.ToInt32(selectCommand.ExecuteScalar());
                return incidentID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

    }
}
    

