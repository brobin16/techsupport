﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportDatabase.Model;

namespace TechSupportDatabase.DAL
{
    public static class CustomerDAL
    {

        public static List<Customer> GetCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string selectStatement =
                "SELECT Customers.Name AS CustomerName, CustomerID " +
                "FROM Customers ";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerName = reader["CustomerName"].ToString();
                                customer.CustomerID = reader["CustomerID"].ToString();
                                 customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return customerList;
        }
    }
}
