﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechSupportDatabase.Model;

namespace TechSupportDatabase.DAL
{
    public static class TechnicianDAL
    {

        public static List<Technician> GetTechnicians()
        {
            List<Technician> technicianList = new List<Technician>();
            string selectStatement =
                "SELECT Technicians.Name AS TechnicianName, TechID " +
                "FROM Technicians ";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Technician technician = new Technician();
                                technician.TechnicianName = reader["TechnicianName"].ToString();
                                technician.TechID = (int)reader["TechID"];
                                technicianList.Add(technician);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return technicianList;
        }

     
        public static List<Technician> GetTechniciansWithOpenIncidents()
        {
            List<Technician> technicianList = new List<Technician>();
            string selectStatement =
                "SELECT DISTINCT Technicians.Name AS TechnicianName, Technicians.TechID, Email, Phone " +
                "FROM Technicians " +
                "JOIN Incidents ON Technicians.TechID = Incidents.TechID " +
                "WHERE DateClosed IS NULL ";
            try
            {
                using (SqlConnection connection = IncidentDBConnection.GetConnection())
                {
                    connection.Open();
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Technician technician = new Technician();
                                technician.TechnicianName = reader["TechnicianName"].ToString();
                                technician.TechID = (int)reader["TechID"];
                                technician.Email = reader["Email"].ToString();
                                technician.Phone = reader["Phone"].ToString();
                                technicianList.Add(technician);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return technicianList;
        }



    }
}
