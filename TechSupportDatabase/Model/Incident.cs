﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TechSupportDatabase.Model
{
    //Declaration of Incident
    public class Incident
    {
        public string TechnicianName { get; set; }
        public string CustomerName { get; set; }

        public string ProductCode { get; set; }

        public DateTime DateOpened { get; set; }

        public string CustomerID { get; set; }

        public int TechID { get; set; }

        public string Title { get; set; }

        public string ProductName { get; set; }
        public string DateClosed { get; set; }
        public string Description { get; set; }

        public int IncidentID { get; set; }

    }
}