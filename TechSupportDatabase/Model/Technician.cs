﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportDatabase.Model
{
    public class Technician
    {
        public int TechID { get; set; }

        public string TechnicianName { get; set; }

        public string Phone { get; set;  }

        public string Email { get; set; }

        public DateTime DateClosed { get; set;  }

    }
}
