﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechSupportDatabase.Model
{
    //Declaration of Incident
    public class Customer
    {
        public string CustomerID { get; set; }

        public string CustomerName { get; set; }
    }
}
